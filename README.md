# Practica de Big Data de MachineLearning101

Esta es mi práctica de Machine Learning 101.
El notebook está en el enlace de abajo. En el propio fichero está explicado todo lo que voy haciendo.
Espero haberme explicado bien. Cualquier duda, por favor, me lo comentas.

## Enlace al notebook de Jupyter Practica_ML_JuanManuel_Iriondo_Ortega.ipynb

https://drive.google.com/file/d/15IzXVYhzDA1dJ7u1snuJYkmQQEbP01I7/view?usp=sharing
